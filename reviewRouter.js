/* eslint-disable no-console */
const express = require('express');

const router = express.Router();

const allComments = new Set([
  {
    // TODO: user camelCase in declaration
    idComment: 123,
    text: 'some text',
  },
  {
    idComment: 15,
    text: 'it\'s stupid, use angular, react or vue!',
  },
  {
    idComment: 22,
    text: 'hey guys u menya vsyo nice',
  },
  {
    idComment: 62,
    text: 'I\'ve become so numb I can\'t feel you there\n' +
    'I\'ve become so tired so much more aware\n' +
    'I\'m becoming this all I want to do\n' +
    'Is be more like me and be less like you\n',
  },
  {
    idComment: 74,
    text: 'ne nu a cho?',
  },
  {
    idComment: 45,
    text: 'I like express, webpack and npm',
  },
]);


// es6 так es6
const createArrayComments = (allComments) => {
  const arrayOfComments = [];
  for (let x of allComments) {
    arrayOfComments.push(x);
  }
  return arrayOfComments;
};

function refreshCommentArray(commentId) {
  allComments.forEach(item => {
    if (item.id_comment === commentId){
      allComments.delete(item);
    }
  })
}

router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  // i'll do it right later
  console.log(`Time: ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}:${new Date().getMilliseconds()}`);
  next();
});

// router.use(timeLogger);
// eslint-disable-next-line no-unused-vars
router.post('/add', (req, res, next) => {
  // TODO: вот это делается проще
  // if (req.body.id_user === 1) {
  //   console.log(req.body.text);
  //   textTo += 'Whaaat? Okay, we moderate your comment:\n' + req.body.text;
  // }
  const textTo = 'Lol!';
  console.log('/add');
  res.json({
    result: 1,
    userMessage: req.body.id_user === 1 ? `${textTo}` : `Whaaat? Okay, we moderate your comment:\n ${req.body.text}`,
  });
});

// eslint-disable-next-line no-unused-vars
router.post('/submit', (req, res, next) => { 
  console.log('/submit');
  res.json({ result: 1 });
});

// eslint-disable-next-line no-unused-vars
router.post('/delete', (req, res, next) => {
  console.log('/delete');
  const commentId = req.body.id_comment;
  refreshCommentArray(commentId);
  res.json({ result: 1 });
});

// prefer arrow functions
// eslint-disable-next-line no-unused-vars
router.post('/list', (req, res, next) => res.json({ comments: createArrayComments(allComments) }));

module.exports = router;
